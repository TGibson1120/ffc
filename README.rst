=============================
FFC: The FEniCS Form Compiler
=============================

FFC is a compiler for finite element variational forms. From a
high-level description of the form, it generates efficient low-level
C++ code that can be used to assemble the corresponding discrete
operator (tensor). In particular, a bilinear form may be assembled
into a matrix and a linear form may be assembled into a vector.  FFC
may be used either from the command line (by invoking the ``ffc``
command) or as a Python module (``import ffc``).

FFC is part of the FEniCS Project.

For more information, visit http://www.fenicsproject.org


Documentation
=============

The FFC documentation can be viewed at Read the Docs:

+--------+-------------------------------------------------------------------------------------+
|FFC     |  .. image:: https://readthedocs.org/projects/fenics-ffc/badge/?version=latest       |
|        |     :target: http://fenics.readthedocs.io/projects/ffc/en/latest/?badge=latest      |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+


License
=======

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
